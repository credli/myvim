set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin() 
" call vundle#rc() " slower, but allows plugins to be configured inline
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" Keep Plugin commands between vundle#begin/end.
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'flazz/vim-colorschemes'
Plugin 'altercation/vim-colors-solarized'
Plugin 'ColorSchemeMenuMaker'
Plugin 'desert-warm-256'
Plugin 'chriskempson/base16-vim'
Plugin 'powerline/powerline'

" ctrlp fuzzy file search
Plugin 'ctrlpvim/ctrlp.vim'

" Easy motion
Plugin 'easymotion/vim-easymotion'

" Neocomplete
Plugin 'Shougo/neocomplete'
Plugin 'Shougo/neosnippet'
Plugin 'Shougo/neosnippet-snippets'

" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'

" Install L9
Plugin 'L9'

" Syntastic
Plugin 'vim-syntastic/syntastic'

" Auto-pairs (brackets, quotes... etc.)
Plugin 'jiangmiao/auto-pairs'

" NerdTree and NERDCommenter plugins
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'jistr/vim-nerdtree-tabs'

Plugin 'terryma/vim-multiple-cursors'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" Airline
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" golang support
Plugin 'fatih/vim-go'

" javascript support
Plugin 'pangloss/vim-javascript.git'
Plugin 'mxw/vim-jsx'
Plugin 'othree/jspc.vim'
Plugin 'flowtype/vim-flow'

" An All of your Plugins must be added before the following line
call vundle#end()            " not required when using vundle#rc()

" To ignore plugin indent changes, instead use:
filetype plugin on
filetype plugin indent on    " required
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"

" timeoutlen is used for mapping delays, and ttimeoutlen is used for key code
" delays, this resolves the issue of delay when exiting insert mode
set timeoutlen=1000 ttimeoutlen=0

" Enable neocomplete on startup
let g:neocomplete#enable_at_startup = 1
let g:acp_enableAtStartup = 0
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#sources#syntax#min_keyword_length = 4
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ neocomplete#start_manual_complete()
function! s:check_back_space() "{{{
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction"}}}
  
" NerdTree tabs toggle command
map <Leader>n <plug>NERDTreeTabsToggle<CR>

" NERDTree tabs
let g:nerdtree_tabs_open_on_gui_startup = 1
let g:nerdtree_tabs_open_on_console_startup = 2
let g:nerdtree_tabs_smart_startup_focus = 1
let g:nerdtree_tabs_open_on_new_tab = 1
let g:nerdtree_tabs_meaningful_tab_names = 1
let g:nerdtree_tabs_synchronize_view = 1
let g:nerdtree_tabs_autofind = 1
let g:nerdtree_tabs_autoclose = 1
let g:nerdtree_tabs_focus_on_files = 1

" NERDCommenter defaults
let g:NERDSpaceDelims = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1

" Neosnippets directory
let g:neosnippet#snippets_directory='~/.vim/bundle/neosnippet-snippets'
" Neosnippets plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
            \ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
    set conceallevel=2 concealcursor=niv
endif

" Enable snipMate compatibility feature.
let g:neosnippet#enable_snipmate_compatibility = 1


" vim-go
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_fmt_command = "goimports"
let g:go_fmt_autosave = 1
let g:go_list_type = "quickfix"
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)
au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dt <Plug>(go-def-tab)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au FileType go nmap <Leader>gb <Plug>(go-doc-browser)
au FileType go nmap <Leader>s <Plug>(go-implements)
au FileType go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>e <Plug>(go-rename)

" Preview autocompletion func parameters
set completeopt+=menuone
set completeopt-=preview
let g:echodoc_enable_at_startup = 1
set cmdheight=2

" Syntastic ESLINT setup
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_loc_list_height = 5
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_javascript_checkers = ['eslint']

let g:syntastic_error_symbol = '❌'
let g:syntastic_warning_symbol = '⚠️'
let g:syntastic_style_warning_symbol = '💩'

" vim-go specific syntastic settings
let g:syntastic_go_checkers = ['golint', 'govet', 'errcheck']
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go']  }

highlight link SyntasticErrorSign SignColumn
highlight link SyntasticWarningSign SignColumn
highlight link SyntasticStyleErrorSign SignColumn
highlight link SyntasticStyleWarningSign SignColumn

" Enable both jsx and js ReactJS syntax highlighting
let g:jsx_ext_required = 0

" Configure Syntastic to use ESLint
let g:syntastic_javascript_checkers = ['eslint']

" ctrlp
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows

let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
" remap ctrl-t to return, this is almost always the wanted behavior
let g:ctrlp_prompt_mappings = {
    \ 'AcceptSelection("e")': ['<c-t>'],
    \ 'AcceptSelection("t")': ['<cr>', '<2-LeftMouse>'],
    \ }

" vim-jsx
let g:jsx_ext_required = 0

" multiple cursors
function! Multiple_cursors_before()
    exe 'NeoCompleteLock'
    echo 'Disabled autocomplete'
endfunction

function! Multiple_cursors_after()
    exe 'NeoCompleteUnlock'
    echo 'Enabled autocomplete'
endfunction

"
" vim settings
" 
let base16colorspace=256  " Access colors present in 256 colorspace
let mapleader="," " remap leader to comma
colorscheme base16-default-dark
syntax on

set number
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab

augroup myfiletypes 
    autocmd!
    autocmd BufNewFile,BufRead *.ajs,*.es6,*.es      setlocal filetype=javascript
    autocmd BufNewFile,BufRead *.m                   setlocal filetype=objc
    autocmd BufNewFile,BufRead *.jade                setlocal filetype=pug
    autocmd BufNewFile,BufRead .babelrc              setlocal filetype=json
    autocmd BufNewFile,BufRead .bowerrc              setlocal filetype=json
    autocmd BufNewFile,BufRead .eslintrc             setlocal filetype=json
    autocmd BufNewFile,BufRead .jscsrc               setlocal filetype=json
    autocmd BufNewFile,BufRead .jshintrc             setlocal filetype=json
    autocmd BufNewFile,BufRead .tmux*.conf*,*.tmux   setlocal filetype=tmux
augroup end

augroup myautocommands
    autocmd!
    " prevent indentation in jade, coffeescript
    autocmd FileType coffee,jade setlocal noautoindent
    autocmd FileType html setlocal matchpairs+=<:>

    " Turn on spell check for certain filetypes automatically
    autocmd BufRead,BufNewFile *.md setlocal spell spelllang=en_us
    autocmd FileType gitcommit setlocal spell spelllang=en_us

    autocmd WinLeave * setlocal nocursorline
    autocmd WinEnter * setlocal cursorline
augroup end

autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
autocmd Filetype json setlocal ts=2 sts=2 sw=2

set encoding=utf-8
set showcmd
set showmatch
set showmode
set incsearch
set ruler
set backspace=2
set incsearch
set hlsearch

if $TMUX == ''
    set clipboard+=unnamed
endif

" disable annoying left and right scrollbars in MacVim
set guioptions-=L
set guioptions-=l
set guioptions-=R
set guioptions-=r 
